# Unique Entity Field

Use the `required` and `single-valued` fields to check the combined unique limit.

# Install

Add above setting into `repositories` of `composer.json`, then run `composer require drupal/unique_entity_field`

```json
{
  "type": "vcs",
  "url": "https://git.drupalcode.org/sandbox/agrozyme-3157167.git",
  "package": {
    "name": "drupal/unique_entity_field",
    "description": "Unique Entity Field"
    }
  }
}
```
