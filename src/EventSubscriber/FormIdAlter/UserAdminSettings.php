<?php
declare(strict_types=1);

namespace Drupal\unique_entity_field\EventSubscriber\FormIdAlter;

use Drupal\core_event_dispatcher\Event\Form\FormIdAlterEvent as Event;
use Drupal\unique_entity_field\UniqueSettingsForm;
use Drupal\user\AccountSettingsForm;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UserAdminSettings
 */
class UserAdminSettings implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return ['hook_event_dispatcher.form_user_admin_settings.alter' => 'alter'];
  }

  /**
   * @param Event $event
   */
  public function alter(Event $event) {
    /** @var AccountSettingsForm $formObject */
    $formObject = $event->getFormState()->getFormObject();

    if (false === $formObject instanceof AccountSettingsForm) {
      return;
    }

    $settings = new UniqueSettingsForm('user', 'user');
    $settings->alter($event);
  }

}
