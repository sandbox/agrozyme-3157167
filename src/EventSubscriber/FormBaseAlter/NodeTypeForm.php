<?php
declare(strict_types=1);

namespace Drupal\unique_entity_field\EventSubscriber\FormBaseAlter;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\core_event_dispatcher\Event\Form\FormBaseAlterEvent as Event;
use Drupal\unique_entity_field\UniqueSettingsForm;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UniqueSettingForm
 */
class NodeTypeForm implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return ['hook_event_dispatcher.form_base_node_type_form.alter' => 'alter'];
  }

  /**
   * @param Event $event
   */
  public function alter(Event $event) {
    /** @var EntityFormInterface $formObject */
    $formObject = $event->getFormState()->getFormObject();

    if (false === $formObject instanceof BundleEntityFormBase) {
      return;
    }

    $entity = $formObject->getEntity();
    $entityType = $entity->getEntityType()->getProvider();
    $bundle = $entity->id();
    //$form = &$event->getForm();

    $settings = new UniqueSettingsForm($entityType, $bundle);
    $settings->alter($event);
  }

}
