<?php
declare(strict_types=1);

namespace Drupal\unique_entity_field;

use Drupal;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\core_event_dispatcher\Event\Form\AbstractFormEvent as Event;

/**
 * Class UniqueValidation
 */
class UniqueValidation extends UniqueBase {
  /**
   * @param Event $event
   */
  public function alter(Event $event) {
    $form = &$event->getForm();
    $form['#validate'][] = [$this, 'validate'];
  }

  /**
   * @param array $element
   * @param FormStateInterface $form_state
   *
   * @return bool
   * @throws MissingDataException
   */
  public function validate(array $element, FormStateInterface $form_state): bool {
    /** @var EntityFormInterface $form */
    $form = $form_state->getFormObject();

    if (false === $form instanceof EntityFormInterface) {
      return true;
    }

    $entity = $form->buildEntity($element, $form_state);
    $fields = $this->getUniqueFields(false);
    $values = $this->getUniqueValues($entity, $fields);
    $scope = $this->getConfig(static::SCOPE);

    if (empty($values)) {
      return true;
    }

    $values['type'] = $this->bundle;
    $query = Drupal::entityQuery($this->entityType);

    foreach ($values as $field => $value) {
      $query = $query->condition($field, $value, '=');
    }

    if ('langcode' === $scope) {
      $data = $entity->getTypedData();
      $query = $query->condition('langcode', $data->get('langcode')->getString(), '=');
    }

    $items = $query->execute();

    if ($this->hasUnique($items, $entity->id())) {
      return true;
    }

    unset($values['type']);
    $field_label = implode(', ', $this->getUniqueLabels($values, $fields));
    $form_state->setErrorByName('', t('The field @field_label has to be unique.', ['@field_label' => $field_label]));
    return false;
  }

  /**
   * @param EntityInterface $entity
   * @param string[] $fields
   *
   * @return string[]
   * @throws MissingDataException
   */
  protected function getUniqueValues(EntityInterface $entity, array $fields): array {
    $data = $entity->getTypedData();
    $items = [];

    foreach ($this->getConfig(static::FIELDS) as $field => $checked) {
      if ($checked && isset($fields[$field])) {
        $items[$field] = $data->get($field)->getString();
      }
    }

    return $items;
  }

  /**
   * @param array $items
   * @param int|null|string $id
   *
   * @return bool
   */
  protected function hasUnique(array $items, $id) {
    if (isset($id)) {
      $items = array_filter($items, function ($item) use ($id) {
        return ($item != $id);
      });
    }

    return empty($items);
  }

  /**
   * @param string[] $values
   * @param string[] $fields
   *
   * @return string[]
   */
  protected function getUniqueLabels(array $values, array $fields) {
    $items = [];

    foreach (array_keys($values) as $field) {
      $items[$field] = $fields[$field];
    }

    return $items;
  }

}
